<?php
//Release 0
class Animal
{
    public $nama;
    public $legs = 4;
    public $cold_blooded = "no";
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
    public function setNama($nama)
    {
        $this->nama = $nama;
    }
    public function setLegs($legs)
    {
        $this->legs = $legs;
    }
    public function setColdBlooded($blood)
    {
        $this->cold_blooded = $blood;
    }
    public function getNama()
    {
        return $this->nama;
    }
    public function getLegs()
    {
        return $this->legs;
    }
    public function getColdBlooded()
    {
        return $this->cold_blooded;
    }
}
