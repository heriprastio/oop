<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

$kodok = new Frog("Buduk");
$kodok->setJumped("Hop Hop");

$sungokong = new Ape("Kera sakti");
$sungokong->setlegs(2);
$sungokong->setYell("Auooo");
$sungokong->setColdBlooded("no");

echo "Name : $sheep->nama <br>";
echo "legs : " . $sheep->getlegs() . "<br>";
echo "cold blooded: " . $sheep->getColdBlooded() . "<br><br>";

echo "Name : $kodok->nama <br>";
echo "legs : " . $kodok->getlegs() . "<br>";
echo "cold blooded: " . $kodok->getColdBlooded() . "<br>";
echo "Jump: " . $kodok->getJumped() . "<br><br>";

echo "Name : $sungokong->nama <br>";
echo "legs : " . $sungokong->getlegs() . "<br>";
echo "cold blooded: " . $sungokong->getColdBlooded() . "<br>";
echo "Yell: " . $sungokong->getYell();
